package com.xj.nrpc.client.handler;

import com.alibaba.fastjson.JSONObject;
import com.xj.nrpc.client.future.DefaultFuture;
import com.xj.nrpc.core.util.ResponseEntity;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class ClientHandler extends ChannelInboundHandlerAdapter {

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		if ("ping".equals(msg.toString())) {
			System.out.println(msg.toString() + " i am client" + System.currentTimeMillis());
			ctx.channel().writeAndFlush("ping\r\n");
			return;
		}
		ResponseEntity resp = JSONObject.parseObject(msg.toString(), ResponseEntity.class);
		DefaultFuture.recive(resp);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
		// super.userEventTriggered(ctx, evt);
	}

}
