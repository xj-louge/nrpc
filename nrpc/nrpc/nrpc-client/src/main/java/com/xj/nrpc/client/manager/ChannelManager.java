package com.xj.nrpc.client.manager;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import io.netty.channel.ChannelFuture;

public class ChannelManager {
	
	public static final CopyOnWriteArrayList<String> realPaths = new CopyOnWriteArrayList<String>();

	public static final CopyOnWriteArrayList<ChannelFuture> channelFutures = new CopyOnWriteArrayList<ChannelFuture>();
	
	public static AtomicInteger index = new AtomicInteger(0);

	/**
	 * 暂时逻辑为给一台服务器发送消息通知，后续添加广播功能
	 * 此处可能存在线程安全问题
	 * @return
	 */
	public static ChannelFuture channelFuturesGet(AtomicInteger i) {
		int futureSize = channelFutures.size();
		ChannelFuture cf = null;
		if(i.get() > futureSize &&futureSize != 0){
			cf = channelFutures.get(i.get());
			index = new AtomicInteger(1);
		}else{
			cf = channelFutures.get(i.getAndIncrement());
		}
		if(cf != null && !cf.channel().isActive()){
			channelFuturesRemove(i);
			channelFuturesGet(i);
		}
		return cf;
	}
	
	public static int channelFuturesSize() {
		return channelFutures.size();
	}

	public static boolean channelFuturesIsEmpty() {
		return channelFutures.isEmpty();
	}

	public static boolean channelFuturesAdd(ChannelFuture e) {
		return channelFutures.add(e);
	}

	public static boolean channelFuturesRemove(Object o) {
		return channelFutures.remove(o);
	}

	public static void channelFuturesClear() {
		channelFutures.clear();
	}
	
	public static int realPathsSize() {
		return realPaths.size();
	}

	public static boolean realPathsIsEmpty() {
		return realPaths.isEmpty();
	}

	public static boolean realPathsAdd(String e) {
		return realPaths.add(e);
	}

	public static boolean realPathsRemove(Object o) {
		return realPaths.remove(o);
	}

	public static void realPathsClear() {
		realPaths.clear();
	}

}
