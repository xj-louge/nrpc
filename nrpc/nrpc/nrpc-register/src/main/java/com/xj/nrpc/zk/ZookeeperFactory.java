package com.xj.nrpc.zk;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class ZookeeperFactory {

	public static CuratorFramework cf;

	public static CuratorFramework create() {
		if (cf != null) {
			return cf;
		}
		RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
		cf = CuratorFrameworkFactory.newClient("127.0.0.1:2181", retryPolicy);
		cf.start();
		return cf;
	}

	public static void main(String[] args) throws Exception {
		CuratorFramework cf = ZookeeperFactory.create();
		cf.create().forPath("/nrpc");
	}

}
