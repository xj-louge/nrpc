package com.xj.nrpc.spring.test.api.controller;

import com.xj.nrpc.core.annotation.Remote;
import com.xj.nrpc.core.util.ResponseEntity;
import com.xj.nrpc.spring.test.api.model.UserModel;

@Remote("userRemote")
public interface UserRemote {

	public ResponseEntity saveUser(UserModel userModel) ;

	public ResponseEntity delUser(String userId) ;
	
}
