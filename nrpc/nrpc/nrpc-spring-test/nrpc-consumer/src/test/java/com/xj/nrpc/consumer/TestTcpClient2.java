package com.xj.nrpc.consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xj.nrpc.core.annotation.RemoteInvoking;
import com.xj.nrpc.spring.test.api.controller.UserRemote;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=TestTcpClient2.class)
@ComponentScan("com.xj")
public class TestTcpClient2 {

	@RemoteInvoking
	private UserRemote user;

	@Test
	public void testTcpClient() {
		// for(int i=0;i<10;i++){
		/*RequestEntity request = new RequestEntity();
		request.setId(Math.random() + "");
		request.setCommond("com.xj.nrpc.user.controller.UserController.delUser");
		request.setData("测试tcp长连接请求");
		ResponseEntity resp = TCPClient.send(request);
		System.out.println(resp.getResult());*/
		user.delUser("测试tcp长连接请求");
		// }
	}

}