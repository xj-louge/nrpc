package com.xj.nrpc.producer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.xj")
public class NRPCSpringProducer {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(NRPCSpringProducer.class);
	}

}