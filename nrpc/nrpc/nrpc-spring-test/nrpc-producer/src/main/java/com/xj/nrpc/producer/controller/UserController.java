package com.xj.nrpc.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.xj.nrpc.core.util.ResponseEntity;
import com.xj.nrpc.producer.server.UserService;
import com.xj.nrpc.spring.test.api.controller.UserRemote;
import com.xj.nrpc.spring.test.api.model.UserModel;

@Controller
public class UserController implements UserRemote {

	@Autowired
	private UserService userService;

	public ResponseEntity saveUser(UserModel userModel) {

		System.out.println("saveUser被调用：" + JSONObject.toJSONString(userModel));
		userService.saveUser();
		ResponseEntity resp = new ResponseEntity();
		resp.setId(Math.random() + "");
		resp.setResult(userModel);
		return resp;
	}

	public ResponseEntity delUser(String userId) {
		System.out.println("delUser被调用：" + JSONObject.toJSONString(userId));
		userService.delUser(userId);
		ResponseEntity resp = new ResponseEntity();
		resp.setId(Math.random() + "");
		resp.setResult(userId);
		return resp;
	}

}
