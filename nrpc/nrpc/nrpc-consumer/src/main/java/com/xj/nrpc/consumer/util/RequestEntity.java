package com.xj.nrpc.consumer.util;

import java.io.Serializable;

public class RequestEntity implements Serializable {

	private static final long serialVersionUID = 8893720268661482490L;

	private String id;

	private String commond;

	private Object data;

	public String getCommond() {
		return commond;
	}

	public void setCommond(String commond) {
		this.commond = commond;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
