package com.xj.nrpc.user.controller;

import com.xj.nrpc.consumer.annotation.Remote;
import com.xj.nrpc.consumer.util.ResponseEntity;
import com.xj.nrpc.user.model.UserModel;

@Remote("userRemote")
public interface UserRemote {

	public ResponseEntity saveUser(UserModel userModel) ;

	public ResponseEntity delUser(String userId) ;
	
}
