package com.xj.nrpc.core.util;

import java.io.Serializable;

public class ResponseEntity implements Serializable {
	private static final long serialVersionUID = -3746989838336297779L;

	private String id;

	private String code = "200";

	private String msg;

	private Object result;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

}
