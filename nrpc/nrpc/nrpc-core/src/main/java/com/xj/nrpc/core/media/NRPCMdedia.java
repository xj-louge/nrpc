package com.xj.nrpc.core.media;

import java.lang.reflect.Method;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.xj.nrpc.core.annotation.Remote;

@Component
public class NRPCMdedia implements BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if (bean.getClass().getInterfaces() != null && bean.getClass().getInterfaces().length != 0) {
			Class<?>[] clzz = bean.getClass().getInterfaces();
			for (Class<?> cl : clzz) {
				if (cl.isAnnotationPresent(Remote.class)) {
					Method[] methods = bean.getClass().getDeclaredMethods();
					Map<String, BeanMethod> map = Media.media_map;
					for (Method m : methods) {
						Class<?>[] cs = bean.getClass().getInterfaces();
						for (Class<?> c : cs) {
							if (c.isAnnotationPresent(Remote.class)) {
								String key = c.getName() + "." + m.getName();
								BeanMethod beanMethos = new BeanMethod();
								beanMethos.setBean(bean);
								beanMethos.setMethod(m);
								map.put(key, beanMethos);
							}
						}
					}
				}
			}
		}
		return bean;
	}
}
