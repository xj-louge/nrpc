package com.xj.nrpc.core.util;

public class DataUtil {

	public static boolean isArray(Object obj) {
		if (obj == null) {
			return false;
		}
		return obj.getClass().isArray();
	}

}
